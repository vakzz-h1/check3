Gem::Specification.new do |s|
  s.name = %q{check}
  s.version = "0.0.1"
  s.author = "William Bowling"
  s.homepage = "https://devcraft.io"
  s.license = "MIT"
  s.date = %q{2021-05-18}
  s.summary = %q{check}
  s.files = [
    "lib/check.rb"
  ]
  s.test_files = Dir.glob("spec/*")
  s.require_paths = ["lib"]
  s.add_runtime_dependency 'git', "~> 1.8.1 "
end
